var express = require('express');
var app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const socket = require('./socket_config');
const path = require('path');
const pug = require('pug');
const sass = require('node-sass');
const sassMiddleware = require('node-sass-middleware');

app.get('/', function(req, res) {
	res.render('index.pug');
});

app.set('views', __dirname + '/src/chat');

app.set('view engine', 'pug');

app.set('view options', {pretty: '    '});


socket.initSocket(io);

app.locals.pretty = true;

app.use(sassMiddleware({ 
    src: __dirname + '/src/chat',
    dest: __dirname + '/public/stylesheets/',
    debug: true,
  	indentedSyntax : false,
  	outputStyle: 'compressed',
  	sourceMap: true
})); 

app.use(express.static('./public'));
 
http.listen(3000, function() {
	console.log('listen on *:3000')
});