function broadcastMessage(io, message) {
	io.emit('chat message', message)
}

module.exports = {
	initSocket : (io) => {
		io.on('connection', (socket) => {

			socket.on('disconnect', (algo) => {
				broadcastMessage(io, '')
			})

			socket.on('chat message', (message) => {
				broadcastMessage(io, message)
			})
			socket.emit('image', { image: true });
		});
	}
}